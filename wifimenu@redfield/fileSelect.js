// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Lang = imports.lang;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GLib = imports.gi.GLib;

const Gettext = imports.gettext.domain('wifimenu@redfield');
const _ = Gettext.gettext;

const RedfieldFileSelectApp = new Lang.Class({
    //A Class requires an explicit Name parameter. This is the Class Name.
    Name: 'RedfieldFileSelectApp',

    _init: function() {
        this.application = new Gtk.Application({
            application_id: 'com.redfield.wifimenu-openfile',
            flags: Gio.ApplicationFlags.FLAGS_NONE,
        });
        this.application.title = 'Redfield Select File';
        GLib.set_prgname(this.application.title);
        GLib.set_application_name(this.application.title);

        this.application.connect('activate', Lang.bind(this, this._onActivate));
        this.application.connect('startup', Lang.bind(this, this._onStartup));
    },

    _buildUI: function() {
        let cTitle = _("Select file");

        this._chooser = new Gtk.FileChooserDialog({title: cTitle,
                                                   action: Gtk.FileChooserAction.OPEN,
                                                   modal: true,
                                                   //transient_for: this.get_toplevel(),
                                                   use_header_bar: false,
                                                  });

        this._chooser.set_current_folder(GLib.get_home_dir());
        this._chooser.set_select_multiple(false);
        this._chooser.set_local_only(true);

        this._chooser.add_button (Gtk.STOCK_CANCEL, 0);
        this._chooser.add_button (Gtk.STOCK_OPEN, 1);
        this._chooser.set_default_response(1);

        this._readonly = new Gtk.CheckButton({label: _("Open file(s) as read-only")});
        this._chooser.set_extra_widget(this._readonly);
    },

    _onActivate: function() {
        if (this._chooser.run () == 1) {
            let files = [];
            let uhome = GLib.get_home_dir();
            let uname = GLib.get_user_name();
            let mpath = GLib.build_pathv('/', ['/media', uname]);
            let found_invalid = false
            for (let i in this._chooser.get_filenames()) {
                let file = this._chooser.get_filenames()[i];
                if (GLib.path_is_absolute(file) == false) {
                    file = GLib.build_pathv('/', [uhome, file]);
                }
                files.push(file);
                /*
                if ((file.indexOf(uhome) == 0) || (file.indexOf(mpath) == 0)) {
                    files.push(file);
                } else {
                    found_invalid = true;
                }
                */
            }
            if (files.length > 0) {
                print(JSON.stringify({
                    'file': files
                    //, 'readonly': this._readonly.get_active()
                }));
            } else if (found_invalid) {
                print(JSON.stringify({
                    'error': _("Only files mounted in your home or removeable medias are allowed")
                }));
            }
        }

        this._chooser.destroy();
    },

    _onStartup: function() {
        this._buildUI();
    }
});

let app = new RedfieldFileSelectApp();
app.application.run(ARGV);
