// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Gio = imports.gi.Gio;
const Lang = imports.lang;
const GLib = imports.gi.GLib;

const netctlIface = '\
<node>\
        <interface name="com.gitlab.redfield.api.netctl">\
                <method name="NetCtlCommand">\
                        <arg direction="in" type="s"/>\
                </method>\
                <signal name="UpdateSignal">\
                        <arg type="s" direction="out"/>\
                </signal>\
        </interface>\
</node>';

const gnomePortalHelperIface = '\
<node>\
  <interface name="org.gnome.Shell.PortalHelper">\
    <method name="Authenticate">\
      <arg name="connection" type="o" direction="in"/>\
      <arg name="url" type="s" direction="in"/>\
      <arg name="timestamp" type="u" direction="in"/>\
    </method>\
    <method name="Close">\
      <arg name="connection" type="o" direction="in"/>\
    </method>\
    <method name="Refresh">\
      <arg name="connection" type="o" direction="in"/>\
    </method>\
    <signal name="Done">\
      <arg type="o" name="connection"/>\
      <arg type="u" name="result"/>\
    </signal>\
  </interface>\
</node>';

var pbtypes = {
    // Update type identifers
    updateState: 0,
    updateIP: 1,
    updateSignal: 2,
    updateAvailNets: 3,
    // update states
    stateDisconnected: 0,
    stateConnected: 1,
    stateConnecting: 2,
    statePortal: 3,
    // Connection action identifiers
    cmdConnect: 0,
    cmdDisconnect: 1,
};

const NetctlIfaceProxy = Gio.DBusProxy.makeProxyWrapper(netctlIface);
const PortalHelperProxy = Gio.DBusProxy.makeProxyWrapper(gnomePortalHelperIface);

var netctlDBusInterface = new Lang.Class({
    Name: "netctlDBusInterface",

    _init: function(menuObject) {
        this.menu = menuObject;
        new NetctlIfaceProxy(Gio.DBus.system, 'com.gitlab.redfield.api.netctl', '/com/gitlab/redfield/api/netctl',
                             (obj, error) => {
                                 if (error) {
                                     global.log("Wi-Fi shell extension", "error launching server proxy");
                                 }
                                 this.server_proxy = obj;
                                 this._updateSignalId = this.server_proxy.connectSignal("UpdateSignal", (proxy, sender, s) => {
                                     this.updateStatus(s);
                                 });
                             });
    },

    updateStatus: function (s) {
        var update = JSON.parse(s);

        this.menu.vmuuid = update.client.uuid;

        switch(update.type) {
        case pbtypes.updateAvailNets:
            this.menu.netlist = update.props.iface.access_points;
            if (update.props.iface.hasOwnProperty("signal_strength")) {
                this.menu.updateIcon(update.props.iface.signal_strength);
            }
            this.menu.iface = update.props.iface.name;
            if (!update.props.hasOwnProperty("state")) {
                break;
            }

        case pbtypes.updateState:
            var state = update.props.state;
            switch (state) {
            case pbtypes.stateConnected: //"connected":
                this.menu.connectedTo(update.props.wifi_conf.ssid);
                this.menu.iface = update.props.iface.name;
                break;
            case pbtypes.stateDisconnected: //"disconnected":
                this.menu.disconnected();
                break;
            case pbtypes.stateConnecting: // connecting
                this.menu.updateNetworkName("connecting");
                this.menu.updateIcon("acquiring");
                break;
            case pbtypes.statePortal: // portal
                this.menu.updateNetworkName("portal");
                // Launch portal login page
                this._launchPortalHelper(update.props.portal_url)
                break;
            default:
                this.menu.updateNetworkName("unkown state");
            }
            break;

        case pbtypes.updateIP:
            this.menu.updateIP(update.props.iface.ip_address);
            break;

        case pbtypes.updateSignal:
            if (update.props.iface.hasOwnProperty("signal_strength")) {
                this.menu.updateIcon(update.props.iface.signal_strength);
            }
            break;
        default:
        }
    },

    sendCommand: function(obj) {
        var s = JSON.stringify(obj);
        this.server_proxy.NetCtlCommandRemote(s);
    },

    _launchPortalHelper: function(url) {
        // Call org.gnome.Shell.PortalHelper
        // Requires arguements: DBus object path, URL, timestamp
        var portalHelper = new PortalHelperProxy(
            Gio.DBus.session,
            'org.gnome.Shell.PortalHelper',
            '/org/gnome/Shell/PortalHelper'
        );

        // Make timestamp
        var ts = Math.round((new Date()).getTime() / 1000);

        // Make the call to org.gnome.Shell.PortalHelper.Authenticate, asynchronously
        portalHelper.AuthenticateRemote(portalHelper.g_object_path, url, ts)
    },

    disable: function() {
        if (this._updateSignalId)
            this.server_proxy.disconnectSignal(this._updateSignalId);
    }
})

